using UnityEngine;

public class IKControl : MonoBehaviour
{
    public bool ikActive;
    public Transform lookObj;
    public Transform rightHandObj;
    Animator anim;
    Transform other;
    public Transform leftFoot;
    public Transform rightFoot;
    public Transform leftToe;
    public Transform leftHeel;
    public Transform rightToe;
    public Transform rightHeel;
    [Range(-1f, 1f)]
    public float footYOffset = 0f;

    Vector3 p1;
    Vector3 p2;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void OnAnimatorIK(int layerIndex)
    {
        if (anim == null) return;

        if (ikActive)
        {
            //anim.bodyRotation = Quaternion.Euler(90f, 0f, 0f);
            if (lookObj != null)
            {
                anim.SetLookAtWeight(0.4f);
                anim.SetLookAtPosition(lookObj.position);
            }
            if (rightHandObj != null)
            {
                anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 0.3f);
                anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 0.3f);
                anim.SetIKRotation(AvatarIKGoal.RightHand, rightHandObj.rotation);
                anim.SetIKPosition(AvatarIKGoal.RightHand, rightHandObj.position);
            }
        }
        else
        {
            anim.SetLookAtWeight(0f);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 0f);
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 0f);
        }
        if (!ikActive) return;
        Ray ray = new Ray(rightFoot.position, Vector3.down);
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(ray, out hit, 0.5f))
        {
            anim.SetIKPositionWeight(AvatarIKGoal.RightFoot, anim.GetFloat("rightFootIK"));
            //anim.SetIKRotationWeight(AvatarIKGoal.RightFoot, 1f);
            anim.SetIKPosition(AvatarIKGoal.RightFoot, hit.point + (footYOffset * Vector3.up));
            //anim.SetIKRotation(AvatarIKGoal.RightFoot, hit.collider.transform.rotation);
            //Debug.DrawRay(rightFoot.position, Vector3.down * 0.5f, Color.red);
        }
        else
        {
            anim.SetIKPositionWeight(AvatarIKGoal.RightFoot, 0f);
            //anim.SetIKRotationWeight(AvatarIKGoal.RightFoot, 0f);
        }
        ray = new Ray(leftFoot.position, Vector3.down);
        hit = new RaycastHit();
        if (Physics.Raycast(ray, out hit, 0.5f))
        {
            Ray r1 = new Ray(leftToe.position, Vector3.down);
            Debug.DrawRay(leftToe.position, Vector3.down, Color.magenta);
            RaycastHit h1 = new RaycastHit();
            Physics.Raycast(r1, out h1, 0.5f);
            p1 = h1.point;
            Ray r2 = new Ray(leftHeel.position, Vector3.down);
            Debug.DrawRay(leftHeel.position, Vector3.down, Color.magenta);
            RaycastHit h2 = new RaycastHit();
            Physics.Raycast(r2, out h2, 0.5f);
            p2 = h2.point; 
            Quaternion lRotation = GetFootRotation(h2.point, h1.point);

            anim.SetIKPositionWeight(AvatarIKGoal.LeftFoot, anim.GetFloat("leftFootIK"));
            anim.SetIKRotationWeight(AvatarIKGoal.LeftFoot, 1f);
            anim.SetIKPosition(AvatarIKGoal.LeftFoot, hit.point + (footYOffset * Vector3.up));
            anim.SetIKRotation(AvatarIKGoal.LeftFoot, lRotation);
            //Debug.DrawRay(rightFoot.position, Vector3.down * 0.5f, Color.red);
        }
        else
        {
            anim.SetIKPositionWeight(AvatarIKGoal.RightFoot, 0f);
            //anim.SetIKRotationWeight(AvatarIKGoal.RightFoot, 0f);
            anim.SetIKPositionWeight(AvatarIKGoal.LeftFoot, 0f);
            //anim.SetIKRotationWeight(AvatarIKGoal.LeftFoot, 0f);
        }
    }

    Quaternion GetFootRotation(Vector3 pos1, Vector3 pos2)
    {
        Vector3 relative = pos2 - pos1;
        return Quaternion.LookRotation(relative);
        //cube.rotation = Quaternion.Euler(xRot, cube.eulerAngles.y, cube.eulerAngles.z);

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(p1, 0.05f);
        Gizmos.DrawWireSphere(p2, 0.05f);
    }
}
