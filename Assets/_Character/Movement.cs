using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class Movement : MonoBehaviour
{
    public float maxSpeed = 2f;
    public float acceleration = 5f;
    public float friction = 4f;
    Vector3 velocity = Vector3.zero;
    CharacterController cc;
    Animator anim;

    void Start()
    {
        cc = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        anim.SetFloat("xspeed", horizontal);
        anim.SetFloat("yspeed", vertical);

        if (Input.GetButtonDown("Jump"))
        {
            anim.SetTrigger("throw");
        }

        velocity = new Vector3(horizontal, 0f, vertical);
        float moveSpeed = velocity.magnitude;
        anim.SetFloat("movespeed", moveSpeed);
        velocity *= maxSpeed;
        //cc.Move(velocity * Time.deltaTime);
        cc.SimpleMove(velocity);
    }

    public void Footstep()
    {
        Debug.Log("clac");
    }
}
