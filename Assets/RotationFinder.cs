using UnityEngine;

public class RotationFinder : MonoBehaviour
{
    Transform pos1;
    Transform pos2;
    Transform cube;

    void Start()
    {
        pos1 = transform.Find("Pos1");
        pos2 = transform.Find("Pos2");
        cube = transform.Find("Cube");
    }

    void Update()
    {
        Vector3 relative = pos2.position - pos1.position;
        float xRot = Quaternion.LookRotation(relative).eulerAngles.x;
        cube.rotation = Quaternion.Euler(xRot, cube.eulerAngles.y, cube.eulerAngles.z);
    }
}
